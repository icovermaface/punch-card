import React, { Component } from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

// Custom Material UI
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';
import Box from '@material-ui/core/Box'
import Button from '@material-ui/core/Button'

// Icons
import Close from '@material-ui/icons/Close';

// Custom Component
import Dialog from '@material-ui/core/Dialog'
import NavPushOut from './components/common/nav-push-out';
import Login from './components/pages/login';

// Desktop View
import LayoutCreateCustomerAccount from './components/layout/layout-customer-create-account'
import HeaderDesktop from './components/common/header-desktop';
import LayoutCustomerProfile from './components/layout/layout-customer-profile';

// Mobile View
import HeaderMobile from './components/common/header';
import Home from './components/pages/home';
import CustomerCreateAccount from './components/pages/customer-create-account'
import CustomerLookup from './components/pages/customer-lookup';
import Scanner from './components/pages/scanner';

// import DesktopView from './components/view/desktop';
import firebase from './firebaseApp'
import './assets/styles/main.scss';
import './App.scss';


const theme = createMuiTheme({
    palette: {
        primary: {
            light: '#666ad1',
            main: '#303f9f',
            dark: '#001970',
            contrastText: '#fff',
          },
          secondary: {
            light: '#ff7961',
            main: '#f44336',
            dark: '#ba000d',
            contrastText: '#000',
          }
        //   https://material.io/resources/color/#!/?view.left=0&view.right=0&primary.color=303F9F&secondary.color=F44336
    },
    typography: {
        fontSize: 16,
        fontFamily: [
            'Nunito Sans',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif'
        ].join(','),
    }
});

const env = firebase.firebaseApp.options

export default class App extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            isProcessing: false,
            camera: "",
            authenticated: true, 
            preloading: false,
            breakpoints: {
                mobile : 768
            },
            isMobile: null,
            dialog: {
                size: "md",
                show: false, 
                type:"",
                data:""
            }
        }
    }
    
    get = (endpoint, params) => {
        if (endpoint === "users") {
            return fetch( env.mongoAPI + "/" + endpoint + "/" + params.id)
            .then(res => res.json())
            .then(
                (result) => {
                    return Promise.resolve(result);
                },
                (error) => {
                    this.setState({
                        slideIn: "",
                        alertMsg: "There is no user for this",
                    });
                    console.log(error)
                }
            )  
        } else if (endpoint === "barcodes") {
            return fetch( env.mongoAPI + "/" + endpoint + "/" + params.id)
            .then(res => res.json())
            .then(
                (result) => {  
                    return Promise.resolve(result);
                },
                (error) => {
                    this.setState({
                        slideIn: "",
                        alertMsg: "There is no user for this",
                    });
                    console.log(error)
                }
            )  
        } else if (endpoint === "customers") {
            let url = null
                if (params !== null) {
                    if(params.customer_id !== null) {
                        // Get One Base on Barcode search 
                        url = env.mongoAPI + "/" + endpoint + "/" + params.customer_id
                    } else {
                         // Get One Base on Customer Search 
                        url = env.mongoAPI + "/" + endpoint + "/" + params._id
                    }
                }
                else {
                     // Get List
                    url = env.mongoAPI + "/" + endpoint
                }
            return fetch(url)
            .then(res => res.json())
            .then(
                (result) => {
                    return Promise.resolve(result);
                },
                (error) => {
                    console.log(error)
                }
            )  
        }
        
    }

    create = (endpoint, params) => { 
        console.log("create", endpoint)
        this.setState({ isProcessing:true });
        if (endpoint === "customers") {
            return window.fetch(env.mongoAPI + "/" + endpoint, {
                method: 'POST',
                body: JSON.stringify(params),
                headers: {
                "Content-type": "application/json; charset=UTF-8"
                }
            })
            .then(res => res.json())
            .then(
                (result) => {
                    console.log("Create", result)
                    this.setState({ isProcessing: false });
                    return Promise.resolve(result);
                },
                (error) => {
                    // this.setState({
                    //     isProcessing: false
                    // });
                    this.setState({ isProcessing: false });
                    console.log(error)
                }
            ) 
        } else if (endpoint === "barcodes") {
            return window.fetch(env.mongoAPI + "/" + endpoint, {
                method: 'POST',
                body: JSON.stringify(params),
                headers: {
                "Content-type": "application/json; charset=UTF-8"
                }
            })
            .then(res => res.json())
            .then(
                (result) => {
                    console.log("Create", result)
                    this.setState({ isProcessing: false });
                    return Promise.resolve(result);
                },
                (error) => {
                    this.setState({ isProcessing: false });
                    console.log(error)
                }
            ) 
        }
    }

    update = (endpoint, params) => { 
        console.log("Endpoint", endpoint)
        console.log("Data", params)
        this.setState({ isProcessing:true });
        let param = {
            _id: params._id,
            barcodes: params.barcodes,
            first_name: params.first_name,
            last_name: params.last_name,
            email: params.email, 
            credits: params.credits
        }
        
        if (endpoint === "customers") {
            return window.fetch(env.mongoAPI + "/" + endpoint + "/" + param._id, {
                method: 'PATCH',
                body: JSON.stringify(param),
                headers: {
                "Content-type": "application/json; charset=UTF-8"
                }
            })
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({ isProcessing: false });
                    return Promise.resolve(result);
                },
                (error) => {
                    this.setState({ isProcessing: false });
                    console.log(error)
                }
            ) 
        } 
    }

    mobileNav = (open) => event => {
        if (event.type === 'keydown' && (event.key === 'Tab' || event.key === 'Shift')) {
            return;
        }

        this.setState({ ...this.state, pushNav: open });
    };

    handleDialogOpen = (params) => {
       console.log(params)
        this.setState({
            dialog: {
                show: true, 
                fullScreen: params.fullScreen,
                size: params.size, 
                type: params.type,
                data: params.data != null ? params.data : null
            }
        });
    };

    handleDialogClose = () => {
        this.setState({ 
            dialog: {
                show: false, 
                fullScreen: false,
                size: false,
                type:"",
                data: ""
        } });
    };

    componentDidMount() {
        window.addEventListener("resize", this.resize.bind(this));
        this.setState({  isMobile: window.innerWidth <= this.state.breakpoints.mobile });
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.resize.bind(this));
    }

    resize() {
        this.setState({
            camera: "reset",
            isMobile: window.innerWidth <= this.state.breakpoints.mobile
        });
    }
    
    render = () => {
        let viewport = this.state.isMobile ? "-mobile" : "-desktop"

            if (viewport === "-mobile") {
                document.body.classList.add("is-mobile");
                document.body.classList.remove("is-desktop");
            } else {
                document.body.classList.remove("is-mobile");
                document.body.classList.add("is-desktop");
            }
       
        return (     
            <ThemeProvider theme={theme}>
                <BrowserRouter basename="/">
                    <Route render={({ location }) => {
                       
                        let modal = null;
                        let nav = null;
                       
                
                        // Device
                        if(this.state.isMobile) {
                            nav = 
                            <HeaderMobile
                                {...this.props} 
                                globalState={this.props.globalState} 
                                mobileNav={this.mobileNav} 
                            />
                        } else {
                            let body = null 
                            if (this.state.dialog.type === "customer-profile") {
                                body = 
                                <React.Fragment>
                                    <Box component="div" m={5}>
                                        <LayoutCustomerProfile 
                                            data={this.state.dialog.data} 
                                            get={this.get} 
                                            update={this.update} 
                                            handleDialogClose={this.handleDialogClose}
                                        />
                                    </Box>
                                </React.Fragment>
                            } else if (this.state.dialog.type === "create-account") {
                                body = 
                                <LayoutCreateCustomerAccount 
                                    {...this.state}
                                    isProcessing={this.state.isProcessing}   
                                    closeDialog={this.handleDialogClose}
                                    create={this.create}
                                />
                            }
                                let classes = ""
                                let closeButton = null 
                                    if (this.state.dialog.fullScreen) {
                                        classes = "--fullscreen"
                                        closeButton = 
                                        <Button className="close-btn" variant="outlined" onClick={this.handleDialogClose}>
                                            <Close />
                                        </Button>
                                    }

                                modal = 
                                <Dialog
                                        className={classes}
                                        fullScreen={this.state.dialog.fullScreen}
                                        fullWidth={true}
                                        maxWidth={this.state.dialog.size}
                                        open={this.state.dialog.show}
                                        onClose={this.handleDialogClose}
                                        aria-labelledby="alert-dialog-title"
                                        aria-describedby="alert-dialog-description"
                                    >
                                    {closeButton}
                                    {body}
                                </Dialog>
                            nav = 
                            <HeaderDesktop 
                                {...this.props} 
                                globalState={this.props.globalState} 
                                mobileNav={this.mobileNav} 
                                create={this.create}
                                handleDialogOpen={this.handleDialogOpen}
                                handleDialogClose={this.handleDialogClose}
                            />
                        }
                        
                        // Authenticaation
                        if (!this.state.authenticated) {
                            return (
                                <React.Fragment key={location} >
                                    <Switch location={location}>
                                            <Route path="/" from={location} exact render={(props) => <Login {...props} {...this.props} from={location} get={this.get} globalState={this.state} />} />
                                            <Route render={() => <Redirect to="/" />} />
                                    </Switch>
                                </React.Fragment>
                            )
                        } else if (location.pathname.startsWith("/admin")) {
                            // NEED TO BUILD ADMIN
                        } else {
                            return (
                                <React.Fragment key={location} >
                                    {nav}
                                    <Switch location={location}>
                                            <Route path="/home" from={location} exact render={(props) => <Home {...props} {...this.props} viewport={viewport} from={location} get={this.get} update={this.update} mobileNav={this.mobileNav} globalState={this.state} handleDialogClose={ this.handleDialogClose} handleDialogOpen={this.handleDialogOpen} openSlider={this.openSlider} />}/>
                                            <Route path="/scanner" from={location} exact render={(props) => <Scanner {...props} {...this.props} from={location} get={this.get} mobileNav={this.mobileNav} globalState={this.state}/>} />
                                            <Route path="/customer-create-account" from={location} exact render={(props) => <CustomerCreateAccount {...props} {...this.props} from={location} get={this.get} create={this.create} globalState={this.state}/>} />
                                            <Route path="/customer-lookup" from={location} exact render={(props) => <CustomerLookup {...props} {...this.props} from={location} get={this.get} globalState={this.state}/>} />
                                            <Route render={() => <Redirect to="/home" />} />
                                    </Switch>
                                    <NavPushOut {...this.props} globalState={this.props.globalState} pushNav={this.state.pushNav} mobileNav={this.mobileNav}/>
                                    {modal}
                                </React.Fragment>
                            )
                        }
                    }}
                    />
                </BrowserRouter>
            </ThemeProvider>
        );
  }
}
