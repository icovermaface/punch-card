import firebase from 'firebase/app';

const config = {
    mongoAPI: process.env.REACT_APP_MONGO_API,
    apiKey: "AIzaSyBbnmSMgIoRx7zrsM-EVFTd-woy0rB6xtk",
    authDomain: "punchcard-c0b56.firebaseapp.com",
    databaseURL: "https://punchcard-c0b56.firebaseio.com",
    projectId: "punchcard-c0b56",
    storageBucket: "punchcard-c0b56.appspot.com",
    messagingSenderId: "286076696313",
    appId: "1:286076696313:web:a277da0bd84ff3dabad428",
    measurementId: "G-7YQJREM38E"

};

const firebaseApp = firebase.initializeApp(config)


export default {
  firebaseApp:firebaseApp
}