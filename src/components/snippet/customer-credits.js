import React, { Component } from 'react';

// Icon
import StarIcon from '@material-ui/icons/Star';
import { Box } from '@material-ui/core';

import "./snippet.scss"

export default class Common extends Component {
    render = () => {
        return (
            <React.Fragment>
                <Box className="customer-credit">
                    {this.props.value}<StarIcon />
                </Box>
           </React.Fragment>
        );
  }
}
