import React, { Component } from 'react';
import Button from '@material-ui/core/Button'
import Quagga from '@ericblade/quagga2';

import KeyboardOutlinedIcon from '@material-ui/icons/KeyboardOutlined';

import '../layout/layout.scss';

export default class LayoutScanner extends Component {

    constructor(props) {
        super(props);

        this.state = {
            nav:{
                menu: true, 
                close:  true
            },
            camera: "", // off, on, reset,
            barcode: {
                code: "",
                success: null , 
                response: {}
            },
            alertMsg: "",
            slideIn: ""
        }   
    }

    componentDidMount = () => {
        this.startCamera()
    }


    componentDidUpdate = (prevProps, prevState, snapshot) => {


        if (this.props.scanner.camera !== prevState.camera) {
            if (this.props.scanner.camera === "off" ) {
                // this.setState({ camera : this.props.scanner.camera  });
                this.stopCamera();
            } else if (this.props.scanner.camera === "on"){
                if (this.state.camera !== "on") {
                    this.startCamera();
                }    
            }

            // if(!this.props.scanner.success) {
            //     this.setState({ alertMsg : "Can't find results" });
            // }
        }

        // if (this.props.scanner.camera === prevState.camera) {
        //     if (!this.props.scanner.success) { 
        //         this.setState({ alertMsg : "Can't find results" });
        //     }
        // }

    }

    componentWillUnmount = () => {
        console.log("unmount")
        this.stopCamera()
    }

    stopCamera = () => {
        Quagga.stop();
        Quagga.offProcessed();
        console.log("stop camera")
        this.setState({ camera : "off" });
    }
    

    startCamera = () => {
        let vWidth = "640"
        let vHeight = "480"

        // let vWidth = this.props.isMobile ? window.innerWidth : window.innerWidth
        // let vHeight = this.props.isMobile ? window.innerWidth : window.innerHeight
        Quagga.init({
            inputStream : {
                name : "Live",
                type : "LiveStream",
                target: document.getElementById("camera_viewport"),
                constraints: {
                    width: vWidth ,
                    height: vHeight
                }
            },
            area: { // defines rectangle of the detection/localization area
                top: "0%",    // top offset
                right: "0%",  // right offset
                left: "0%",   // left offset
                bottom: "0%"  // bottom offset
            },
            locate: true,
            decoder: {
                readers: ["code_128_reader", "upc_reader", "upc_e_reader"] //code_39_reader + code_39_vin_reader
            }
          }, function(err) {
                if (err) {
                    console.log(err);
                    return
                }
                console.log("Initialization finished. Ready to start");
                this.setState({ camera : "on" }, Quagga.start())
                
        }.bind(this));
        
        Quagga.onDetected(function(res) {
            // this.stopCamera();
            if (this.state.camera != "off") {
                let params = {
                    id: res.codeResult.code,
                    format: res.codeResult.format
                }
                
                this.setState({ 
                    ...this.state.barcode,
                    barcode: {
                        code: res.codeResult.code
                    },
                    alertMsg : ""
                }, this.props.onDetect(params));
            }
            
        }.bind(this));
    }


    render() {
        let scannerMsg = null
            
            if (!this.props.scanner.barcode.success) {
                scannerMsg = 
                <div className="scanner-msg">
                    {this.props.scanner.barcode.response.msg}
                </div>
            } else {
                scannerMsg = null
            }
        return (
            <React.Fragment>
                <div id="l-scanner" >
                    <div className="scanner-camera">
                        <div className="camera-overlay_top"></div>
                        <div id="camera_viewport"></div>
                        <div className="camera-overlay_bottom"></div>
                    </div>

                    {scannerMsg}

                    <div className="footer_controller dark-theme" >
                        <Button className="keyboard" onClick={this.props.customerLookup} >
                            <KeyboardOutlinedIcon />
                        </Button>
                    </div>
                </div>
            </React.Fragment>
        );
    }
    
}