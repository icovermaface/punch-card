import React, { Component } from 'react';
import Preloader  from '../snippet/preloader'
import Container from '@material-ui/core/Container'
import Box from '@material-ui/core/Box'
import LinearProgress from '@material-ui/core/LinearProgress';


import CustomerInformation from '../layout/customer-create-account/customer-information'
import CustomerCredits from '../layout/customer-create-account/customer-credits'
import CustomerItems from '../layout/customer-create-account/customer-items'
import CustomerConfirmation from '../layout/customer-create-account/customer-confirmation'

import '../layout/layout-customer-create-account.scss';


export default class LayoutCustomerCreateAccount extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            progressBar : 0,
            steps: [
                // This is the order of the flow that will appear on the frontend
                "customer-information",
                "customer-credits",
                "customer-items",
                "customer-confirmation"
            ],
            customer: {
                first_name:"",
                last_name:"",
                email:"",
                credits:"",
                items: []            
            },
            currentStep: "customer-information"
        }
    }

    componentDidMount() {
        document.body.classList.add('customer_create-account')
    }
    
    componentWillUnmount() {
        document.body.classList.remove('customer_create-account')
    }

    handleCustomerItems = (item) => {
        this.setState({
            customer : {
                ...this.state.customer,
                ["items"]: item
            }
        }, this.handleSubmit);
    }

    handleSubmit = (e) => {
        // e.preventDefault();
        
        // let steps = Object.values(this.state.step)
        let currentStep = this.state.currentStep
        let nextStep = ""
        
        console.log("subnit")
        
        this.state.steps.forEach(function(step, idx){
            if(step === currentStep && this.state.steps.indexOf(currentStep) === this.state.steps.length - 2) {
                this.setState({processing: true}) 
                
                const customerParams = {
                    first_name: this.state.customer.first_name,
                    last_name: this.state.customer.last_name,
                    email: this.state.customer.email,
                    credits: this.state.customer.credits,
                    barcode: this.state.customer.items
                }
                
                this.props.create("customers", customerParams).then(response => {
                    if(response.message === "success") {
                        let result = response.result;
                        result.barcode.forEach(function(barcode) {
                            const barcodeParams = {
                                _id: barcode.barcode.toString(),
                                format: barcode.format,
                                name: barcode.name,
                                customer_id: result._id,
                                customer_name: result.first_name,
                                email: result.email,
                            }
                            this.props.create("barcodes", barcodeParams)
                        }.bind(this))
                        
                        this.setState({
                            processing: false,
                            currentStep: "customer-confirmation"
                        })

                    }
                })

            } else if (currentStep === step) {
                nextStep = this.state.steps[idx+1]
                this.setState({
                    disabledButton: true,
                    currentStep: nextStep
                }) 
                return true;
            }
        }.bind(this))
    }

    handleInputChange = (e) => {
        e.preventDefault();
        const inputName = e.target.name;
        const inputValue = e.target.value;
        this.setState({
            customer : {
                ...this.state.customer,
                [inputName]: inputValue
            }
        });

    }

    progressBar = () => {
        // let steps = Object.values(this.state.step);
        let percentage = 0; 

            this.state.steps.forEach(function(step, idx){
                if (this.state.currentStep === "start") {
                    percentage = 0 ;
                } else if (this.state.currentStep === "thank_you") {
                    percentage = 100 ;
                }  else if (step === this.state.currentStep) {
                    percentage = (idx + 1) /  (this.state.steps.length) * 100 ;
                } 
            }.bind(this))

        return <LinearProgress variant="determinate" value={percentage} />
    }

    rederFlow = () => {
        let currentStep = this.state.currentStep

        let output = null;

            if (currentStep === "customer-credits") {
                output = 
                <CustomerCredits
                    handleSubmit={this.handleSubmit} 
                    handleInputChange={this.handleInputChange} />
            } else if(currentStep === "customer-items") {
                output = 
                <CustomerItems
                    handleSubmit={this.handleSubmit} 
                    handleInputChange={this.handleInputChange}
                    handleCustomerItems = {this.handleCustomerItems} />
            } else if(currentStep === "customer-confirmation") { 
                output = 
                <CustomerConfirmation
                    handleSubmit={this.handleSubmit} 
                    handleInputChange={this.handleInputChange}
                    customer={this.state.customer} />
            } else {
                output = 
                <CustomerInformation 
                    handleSubmit={this.handleSubmit} 
                    handleInputChange={this.handleInputChange} />
            }
        return output
    }

    render = () => {
        if (this.state.referral === "") {
            return <Preloader />;
        }  else {
            return (
                <React.Fragment>
                    <Box className="step-wrapper">
                        <Box className={"step step-" + this.state.currentStep}>
                            <Container maxWidth="sm" >
                                <form onSubmit={this.handleSubmit}>
                                    {this.rederFlow()}
                                </form>
                            </Container>
                        </Box>
                        {this.progressBar()}
                    </Box>
                </React.Fragment>
            )
        }
    }
}
