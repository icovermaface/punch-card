import React, { Component } from 'react';
import CustomerCredit from '../snippet/customer-credits'
import Dialog from '@material-ui/core/Dialog'
import Box from '@material-ui/core/Box'
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import List from '@material-ui/core/List';
import LayoutCustomerProfile from './../layout/layout-customer-profile';



export default class LayoutCustomerList extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            isProcessing: false,
            customer: [],
            dialog: {
                show: false, 
                type:"",
                data:""
            }
        }
    }
    
    componentDidMount = () => {

        if(this.props.customers === undefined) {
            this.props.get("customers", null).then(customer => {
                this.setState({
                    customer: customer
                });
            });
        } 
    }

    handleListClick = (customer) => {
        this.props.history.push({
            pathname: "/customer/" + customer.id,
            state: {
                customer
            }
        });
    }
  

    renderList = () => {
        let list = [];
        
        this.props.customers.forEach(customer => {
            let name = customer.first_name + " " + customer.last_name
            let dialogParam = {
                fullScreen: false,
                size: "xs", 
                type: "customer-profile",
                data: customer
            }
            list.push(
                <ListItem key={customer._id} onClick={this.props.handleDialogOpen.bind(this, dialogParam)}>
                  <ListItemText
                    primary={name}
                    secondary={customer.email}
                  />
                  <ListItemSecondaryAction>
                    <CustomerCredit value={customer.credits} />
                  </ListItemSecondaryAction>
                </ListItem>
            )
        });

        return list
    }

    render = () => {
           
        return (
            <React.Fragment>    
                <Box component="section" m={0} className="layout-body">
                    {/* <TextField fullWidth id="outlined-search" label="Search field" type="search" variant="outlined" /> */}
                    <List component="ul" className="search_results">
                        {this.renderList()}
                    </List>
                </Box>
           </React.Fragment>
        );
  }
}
