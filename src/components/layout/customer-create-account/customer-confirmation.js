import React, { Component } from 'react';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography';

export default class CreateAccountCustomerConfirmation extends Component {

    render = () => {
        return (
            <React.Fragment>
                <Box className="header" >
                    {/* <Typography align="center" variant="subtitle1" component="h2">
                        Customer
                    </Typography>  */}
                    <Typography align="center" variant="h2" component="h1">
                        Account Created
                    </Typography>  
                    <Typography align="center" variant="body2" component="p">
                        Congrats! <strong>{this.props.customer.first_name}</strong> account has been created  
                    </Typography>   
                </Box>  
    
               
                <Grid
                    className="navigation"
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    >
                    
                    <Button variant="contained" color="primary" size="medium" onClick={this.props.handleSubmit}>
                        New Account
                    </Button>
                    <Button variant="text" color="primary" size="medium" onClick={this.props.handleSubmit}>
                        Finish
                    </Button>
                </Grid> 
            </React.Fragment>
        )
    }
}
