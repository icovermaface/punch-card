import React, { Component } from 'react';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography';

export default class CreateAccountCustomerCredits extends Component {

    render = () => {
        return (
            <React.Fragment>
                <Box className="header" >
                    <Typography align="center" variant="subtitle1" component="h2">
                        Customer
                    </Typography> 
                    <Typography align="center" variant="h2" component="h1">
                        Credits
                    </Typography>    
                </Box>  
    
                <FormControl  fullWidth>
                    <TextField
                        defaultValue="0"
                        required
                        type="number"
                        autoFocus
                        name="credits"
                        id="credits"
                        className="input_credits"
                        onChange={this.props.handleInputChange}
                    />
                    {/* <FormHelperText>Disabled</FormHelperText> */}
                </FormControl> 
           
                <Grid
                    className="navigation"
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    >
                    <Button variant="contained" color="primary" size="medium" onClick={this.props.handleSubmit}>
                        Next
                    </Button>
                </Grid> 
            </React.Fragment>
        )
    }
}
