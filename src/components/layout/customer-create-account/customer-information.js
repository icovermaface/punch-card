import React, { Component } from 'react';
import FormControl from '@material-ui/core/FormControl';
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box'
import Typography from '@material-ui/core/Typography';

export default class CreateAccountCustomerInformation extends Component {

    render = () => {
        return (
            <React.Fragment>
                <Box className="header" >
                    <Typography align="center" variant="subtitle1" component="h2">
                        Customer
                    </Typography> 
                    <Typography align="center" variant="h2" component="h1">
                        Information
                    </Typography>    
                </Box>  
    
                <FormControl variant="outlined" fullWidth>
                    <TextField
                        required
                        variant="outlined"
                        fullWidth
                        name="first_name"
                        label="First Name"
                        id="first_name"
                        onChange={this.props.handleInputChange}
                    />
                    {/* <FormHelperText>Disabled</FormHelperText> */}
                </FormControl>
    
                <FormControl variant="outlined" fullWidth>
                    <TextField
                        required
                        variant="outlined"
                        fullWidth
                        name="last_name"
                        label="Last Name"
                        id="last_name"
                        onChange={this.props.handleInputChange}
                    />
                    {/* <FormHelperText>Disabled</FormHelperText> */}
                </FormControl>
    
                <FormControl variant="outlined" fullWidth>
                    <TextField
                        required
                        variant="outlined"
                        fullWidth
                        name="email"
                        label="E-Mail"
                        id="email"
                        type="email"
                        onChange={this.props.handleInputChange}
                    />
                    {/* <FormHelperText>Disabled</FormHelperText> */}
                </FormControl>
                <Grid
                    className="navigation"
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    >
                    <Button variant="contained" color="primary" size="medium" onClick={this.props.handleSubmit}>
                        Next
                    </Button>
                </Grid> 
            </React.Fragment>
        )
    }
}
