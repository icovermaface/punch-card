import React, { Component } from 'react';
import FormControl from '@material-ui/core/FormControl';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';

import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid'
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box'
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';


export default class CreateAccountCustomerItems extends Component {
    
   

    constructor(props) {
        super(props);
        
        const defaults = {
            type: "skate",
            format: "128"
        }
        
        this.state = {
            itemType: defaults.type,
            items : [{
                type: defaults.type,
                name: defaults.type + " 1",
                barcode: "",
                format: defaults.format
            }],
            expanded : 0
        }
    }

    handleChange = (panel) => {
        this.setState({
            expanded: panel
        });
    };

    // handleInputChange = (e) => {
    //     e.preventDefault();
    //     const inputName = e.target.name;
    //     const inputValue = e.target.value;
    //     this.setState({[inputName]: inputValue});
    // }

    handleInputChange = (e) => {
        e.preventDefault();
        const inputName = e.target.name;
        const inputValue = e.target.value;
        const inputID = e.target.id.split(inputName).pop();

        let items = this.state.items
            
            this.state.items[inputID] = {
                ...this.state.items[inputID],
                [inputName]: inputValue
            }

            this.setState({
                items: items
            });
    };

    addItem =()=> {
        let output = this.state.items;
            output.push({
                type: this.state.itemType,
                name: this.state.itemType + " " + (this.state.items.length + 1),
                format: this.state.format,
                barcode: ""
            })

            this.setState({
                items: output,
                expanded : output.length - 1
            });

    }

    subtractItem = (i) => {
        const   output = this.state.items;
                output.splice(i,1);

                this.setState({
                    items: output,
                    expanded : output.length 
                });

                console.log(i - 1)
            console.log(this.state.items) 
    }

    renderItemsList = () => {
        let list = [];

        for (let j=0; j<this.state.items.length; j++) {
            let idx = j;

            let removeButton = null; 
                if (idx > 0) {
                    removeButton = 
                    <Button varient="link" className="item_remove-button" onClick={this.subtractItem.bind(this, idx)}>
                            Remove {this.state.itemType} 
                    </Button> 
                }

            let name  = this.state.itemType + " " + (idx + 1)
                if (this.state.items[idx].name != "") {
                    name = this.state.items[idx].name
                }

            list.push(
                <ExpansionPanel key={idx} className="item" expanded={this.state.expanded === idx} onChange={this.handleChange.bind(this, idx)}>
                    <ExpansionPanelSummary
                        aria-controls= {idx + "-content"}
                        id={idx + "-content"}
                    >
                        <Typography className="header">{name}</Typography>
                        {removeButton}
                        {/* <Typography className="header-secondary">I am an expansion panel</Typography> */}
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Box className="item_form">
                            <FormControl variant="outlined" fullWidth>
                                {/* <InputLabel id={"type-label-" + idx}>Type</InputLabel> */}
                                <Select
                                    name="type"
                                    onChange={this.handleInputChange}
                                    labelId={"type-label-" + idx}
                                    id={"type" + idx}
                                    value={this.state.itemType}
                                >
                                <MenuItem value="">
                                    <em>None</em>
                                </MenuItem>
                                <MenuItem value={"skate"}>Skate</MenuItem>
                                {/* <MenuItem value={"hockey-stick"}>Hockey Stick</MenuItem> */}
                                </Select>
                            </FormControl>
                
                            <FormControl variant="outlined" fullWidth>
                                <TextField
                                    required
                                    variant="outlined"
                                    fullWidth
                                    name="name"
                                    label="Name"
                                    defaultValue={name}
                                    id={"name" + idx}
                                    onChange={this.handleInputChange}
                                />
                                {/* <FormHelperText>Disabled</FormHelperText> */}
                            </FormControl>

                            <FormControl variant="outlined" fullWidth>
                                <TextField
                                    required
                                    variant="outlined"
                                    fullWidth
                                    name="barcode"
                                    label="Barcode"
                                    id={"barcode" + idx}
                                    type="number"
                                    onChange={this.handleInputChange}
                                />
                                {/* <FormHelperText>Disabled</FormHelperText> */}
                            </FormControl>
                        </Box>
                    </ExpansionPanelDetails>
                    <Button varient="link" className="item_add-button " onClick={this.addItem}>
                        + Add {this.state.itemType} 
                    </Button> 
                </ExpansionPanel>
            )
        }

        return list
    }

    render = () => {
        return (
            <React.Fragment>
                <Box className="header" >
                    <Typography align="center" variant="subtitle1" component="h2">
                        Customer
                    </Typography> 
                    <Typography align="center" variant="h2" component="h1">
                        Items
                    </Typography>    
                </Box>  
                {this.renderItemsList()}
                {/* <Box mb={2} className="create-account_items">
                        <Typography  variant="subtitle2" noWrap >
                            Skates
                        </Typography>
                        <Grid container spacing={2}>
                            {this.renderItemsList()}       
                            <Grid item xs={12} >     
                                
                            </Grid>
                        </Grid>
                    </Box> */}
           
                <Grid
                    className="navigation"
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                    >
                    <Button variant="contained" color="primary" size="medium" onClick={this.props.handleCustomerItems.bind(this, this.state.items)}>
                        Create Account
                    </Button>
                </Grid> 
            </React.Fragment>
        )
    }
}
