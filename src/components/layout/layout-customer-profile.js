import React, { Component } from 'react';
import Box from '@material-ui/core/Box'
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import FolderIcon from '@material-ui/icons/Folder';
import Button from '@material-ui/core/Button';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';

// Icon
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import StarIcon from '@material-ui/icons/Star';

import './layout.scss';
import './layout-customer-profile.scss';

export default class LayoutCustomerProfile extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            isProcessing: false,
            onSubmit: {
                disableButton: true,
                response: ""
            },
            barcodes_selected: [],
            barcodes: [],
            first_name: "",
            last_name: "",
            _id: "",
            email: "",
            credits: 0,
            credits_reserved: []
        }
    }

    toggledBarcode = (e) => {
        e.preventDefault();
        const inputName = e.target.name;
        const inputValue = e.target.value;


        // credits
        let creditsReserved = this.state.credits_reserved;
        const idxCreditsReserved = creditsReserved.indexOf(inputName);
                
            console.log(inputValue)
            if (idxCreditsReserved >= 0) {
                creditsReserved.splice(idxCreditsReserved, 1);
                this.increaseCredit(this.state.credits, 1)
            } else if (inputValue === "returned") {
                creditsReserved.push(inputName);
                this.deductCredit(this.state.credits, 1)
            } 
        
        // reservedToggle
        let selectedBarcode = this.state.barcodes_selected;
        const idxSelectedBarcode = selectedBarcode.indexOf(inputName);
                if (idxSelectedBarcode >= 0) {
                    selectedBarcode.splice(idxSelectedBarcode, 1);
                } else {
                    selectedBarcode.push(inputName);
                }

        // barcode
        let barcode = [...this.state.barcodes];
        let barcodeIndex = barcode.findIndex((obj => obj.barcode === inputName));
            barcode[barcodeIndex].status = inputValue
            console.log(this.state.barcodes_selected, barcode)

        // Button
        let onSubmitButton = null 
            if (selectedBarcode.length > 0) {
                onSubmitButton = false
            } else {
                onSubmitButton = true
            }

            this.setState({
                barcodes : barcode,
                barcodes_selected : selectedBarcode,
                onSubmit : {
                    ...this.state.onSubmit,
                    disableButton: onSubmitButton
                }
            });
    }


    deductCredit = (current , num) => {
        this.setState({
            credits: (current - num )
        });
    }

    increaseCredit = (current , num) => {
        this.setState({
            credits: (current + num)
        });
    }


    componentDidMount = () => {
        if (this.props.data !== null) {
            let customer = this.props.data

            this.setState({
                    _id: customer._id,
                    email: customer.email,
                    first_name : customer.first_name, 
                    last_name : customer.last_name, 
                    // barcodes_reserved : customer.barcodes,
                    barcodes: customer.barcodes,
                    credits: customer.credits
            });
        }
        
    }

    renderItems = () => {
        let list = [];
        this.state.barcodes.forEach(function (barcode, idx) {
            let status = ""; 
            let checked = null;
            let labelClass = "";
            
                if (barcode.status === null || barcode.status === "returned" || barcode.status === false ) {
                    status = "in_service" // returned
                    checked = false;
                } else if (barcode.status === "in_service" || barcode.status === true) {
                    status = "returned"
                    labelClass = "--in-service"
                    checked = true;
                } 
            
            list.push(
                    <ListItem key={idx} disableGutters={true}>
                        <ListItemAvatar>
                            <Avatar>
                                <FolderIcon />
                            </Avatar>
                        </ListItemAvatar>
                        <ListItemText
                            className="user_skate-id"
                            primary={barcode.barcode}
                        />
                        <ListItemSecondaryAction>
                        <FormControlLabel
                                className={"user_status " + labelClass}
                                control={
                                    <Switch
                                        id={idx.toString()}
                                        name={barcode.barcode}
                                        checked={checked}
                                        onChange={this.toggledBarcode}
                                        value={status}
                                        color="primary"
                                    />
                                }
                                label="In Service"
                                labelPlacement="bottom"
                            />
                           
                        </ListItemSecondaryAction>
                    </ListItem>
            )
        }.bind(this));

        return list
    }

    onSave = () => {
        this.setState({
            isProcessing : true,
            onSubmit : {
                ...this.state.onSubmit,
                disableButton: true
            }
        });
        this.props.update("customers", this.state).then(response => {
            console.log("Profile Saved")
            if(response.ok !== undefined) {
                this.setState({
                    isProcessing : true,
                    onSubmit : {
                        ...this.state.onSubmit,
                        response: "success"
                    }
                });
            } else {
                this.setState({
                    isProcessing : false,
                    onSubmit : {
                        ...this.state.onSubmit,
                        disableButton: false
                    }
                });
                console.log("error in saving", response)
            }
        })

        
    }

    renderFormSuccess = () => {
        return (
            <React.Fragment>

                <Box component="section" m={0} className="layout-footer layout-footer_center">
                    <Typography  variant="h5" noWrap className="layout-header_title">
                               success
                    </Typography>
                    <Button
                        fullWidth
                        variant="contained"
                        color="primary"
                        size="large"
                        onClick={this.props.handleDialogClose}
                    >
                    Close
                    </Button>
                </Box>
            </React.Fragment>
        )
    }

    renderForm = () => {
        let customerName =  this.state.first_name + " " + this.state.last_name
        let saveButton = "Save"
            
            if (this.state.isProcessing) {
                saveButton = "saving..."
            }
        
        return(   
            <React.Fragment> 
                <Box component="section" m={0} className="layout-header">
                    <Grid container spacing={1}>
                        <Grid item xs={9} className="customer_info">
                            <Typography variant="subtitle2" noWrap className="layout-header_sub-title">
                                {this.state.email}
                            </Typography>
                            <Typography  variant="h5" noWrap className="layout-header_title">
                                {customerName}
                            </Typography>
                        </Grid>
                        <Grid item xs={3} className="customer_credit">
                            <Box component="div" className="customer-credit">
                                {this.state.credits}<StarIcon />
                            </Box>
                        </Grid>
                    </Grid>
                </Box>

                <Box component="section" m={0} className="layout-body">
                    <Typography  variant="subtitle2" noWrap >
                        Skates ({this.state.barcodes.length})
                    </Typography>
                    <List className="skate-results">
                        {this.renderItems()}
                    </List>
                </Box>

                <Box component="section" m={0} className="layout-footer layout-footer_center">
                    <Button
                        disabled={this.state.onSubmit.disableButton}
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        size="large"
                        onClick={this.onSave}
                    >
                    {saveButton}
                    </Button>
                    {/* <Typography  variant="subtitle2" className="layout-footer_view-more" noWrap >
                    <span className="view-more_title">History</span> 
                    <IconButton edge="end" aria-label="Chevron" className="view-more_icon" >
                            <KeyboardArrowDownIcon fontSize="small"/>
                        </IconButton>
                    </Typography> */}
                </Box>
            </React.Fragment>  
        )
    }

    render() {

        let output = null;
            
            if (this.state.onSubmit.response === "success") {
                output = this.renderFormSuccess()
            } else {
                output = this.renderForm()
            }
            
        return output
    }
    
}