import React, { Component } from 'react';
import { Link } from "react-router-dom";

import Drawer from '@material-ui/core/Drawer';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';


// CSS
import './nav-push-out.scss';

export default class NavPushOut extends Component {

    constructor(props) {
        super(props);
        this.state = {
            links: [
                {title:"Scanner", name:"scanner", link:"/scanner"},
                {title:"Create Account", name:"Create Acccount", link:"/customer-create-account"},
                {title:"Order Sticker", name:"order-sticker", link:""},
                {title:"Settings", name:"settings",link:""}
            ]
        }
    }

    track = (link) => {
      
    }

    render() {
        return (
            <React.Fragment>
                 <Drawer open={this.props.pushNav} onClose={this.props.mobileNav(false)} className="push-out-menu">
                    {/* <Grid container spacing={1} className="push-out-menu_header">
                        <Grid item xs={2} >   
                        <IconButton  color="inherit" aria-label="menu" onClick={this.props.mobileNav(false)}>
                            <ArrowBackIosOutlinedIcon />
                        </IconButton> 
                        </Grid>                    
                        <Grid item xs={10} > 
                            <p className="header_company-name">The Company</p>
                            <p className="header_store">Store1</p>
                        </Grid>             
                    </Grid> */}
                    <Grid container spacing={1}>
                        <Grid item xs={12} > 
                            <div className="push-out-menu_header">
                                <div className="header_company-logo"></div>
                                <div>
                                    <p className="header_company-name">The Company</p>
                                    <p className="header_store">Store1</p>
                                </div>
                            </div>
                            <div
                            //   className={classes.list}
                            role="presentation"
                            onClick={this.props.mobileNav(false)}
                            onKeyDown={this.props.mobileNav(false)}
                            className="push-out-menu_list"
                            >
                                <List>
                                    {this.state.links.map((link, index) => (
                                        <ListItem button key={link.title} onClick={this.track.bind(this, link)}>
                                            <Link to={link.link}><ListItemText primary={link.title} /></Link>
                                            
                                        </ListItem>
                                    ))}
                                </List>
                            </div>

                            <div className="push-out-menu_footer">
                                <List>
                                   
                                        <ListItem button onClick={this.track.bind(this, "/logout")}>
                                            <Link to={"link.link"}><ListItemText primary={"Log Out"} /></Link>
                                        </ListItem>
                    
                                </List>
                                
                            </div>
                        </Grid>             
                    </Grid>
                </Drawer>
            </React.Fragment>
        );
    }
}

