import React, { Component } from 'react';
import Form from 'react-bootstrap/Form';
import Button from 'react-bootstrap/Button'
import FormControl from 'react-bootstrap/FormControl';

import './search-bar.scss';

export default class searchBar extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            isProcessing: false,
            autocomplete: ""
        }
    }

    handleInputChange = (e) => {
        e.preventDefault();
        const inputName = e.target.name;
        const inputValue = e.target.value;
        this.setState({[inputName]: inputValue});
    }

    autocomplete = (e) => {    
        e.preventDefault();
        fetch("https://jsonplaceholder.typicode.com/users")
        .then(res => res.json())
        .then(
            (result) => {
                result.forEach(el => {
                    if(el["username"] === this.state.autocomplete) {
                        this.props.history.push({
                            pathname: "/customer/" + el["id"],
                            state: {
                                el
                            }
                        });
                    } else {
                        console.log("no result")
                    }
                });
            // this.setState({
            //     isLoaded: true,
            //     items: result.items
            // });
            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            (error) => {
                console.log(error)
                // this.setState({
                //     isLoaded: true,
                //     error
                // });
            }
        )  
        console.log('autocomplete')
    }

    render() {
        return (
            <Form inline className="search-bar" onSubmit={this.autocomplete}>
                <FormControl type="text" name="autocomplete" placeholder="Customer Look-Up" className="mr-sm-2" onChange={this.handleInputChange}/>
                <Button type="submit" variant="link">icon</Button>
            </Form>
        );
    }
    
}

