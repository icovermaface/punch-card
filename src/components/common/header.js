import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';

// Icons
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import './header.scss';

export default class Header extends Component {
    
    render() {
        let closeButton = null ;

        if(this.props.close !== undefined) {
            closeButton = 
            <IconButton edge="end" color="inherit" aria-label="menu" onClick={this.props.close}>
                <CloseIcon />
            </IconButton>
        }
        return (
            <AppBar position="fixed" className="header-main" >
                <Toolbar>
                    <IconButton edge="start" color="inherit" aria-label="menu" onClick={this.props.mobileNav(true)}>
                        <MenuIcon />
                    </IconButton>
                    <div className="header-main_controller">

                        {closeButton}
                        
                    </div>
                </Toolbar>
            </AppBar>
        );
    }
}

