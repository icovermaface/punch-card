import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';


// Icons
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

// Component

import './header.scss';

export default class Header extends Component {
    constructor(props) {
        super(props);

        this.state = {
            nav:{
                menu: true, 
                close:  true
            },
            dialog: true,
            scannerStart : false, 
            scannerBarcode: "",
            alertMsg: "",
            slideIn: "search"
        }
    }

    render() {
        let dialogParam = {
            fullScreen: true,
            size: "xs", 
            type: "create-account",
            data: null
        }

        return (
            <React.Fragment >
                <AppBar position="fixed" className="header-main" >
                    <Toolbar>
                        <IconButton edge="start" color="inherit" aria-label="menu" onClick={this.props.mobileNav(true)}>
                            <MenuIcon />
                        </IconButton>
                        <div className="header-main_controller">
                            <Button 
                                variant="contained" 
                                color="primary"
                                size="small"
                                onClick={this.props.handleDialogOpen.bind(this, dialogParam)}
                            >
                                Create Account
                            </Button>
                        </div>
                    </Toolbar>
                </AppBar>
            </React.Fragment>
        );
    }
}

