import React, { Component } from 'react';
import './footer.scss';

export default class Footer extends Component {

    render() {
        return (
            <footer className="text-center">
                <p>Powered by <a href="google.com" alt="punchcard company">punchcard</a></p>
            </footer>
        );
    }
    
}

