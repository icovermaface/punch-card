import React, { Component } from 'react';
import PropTypes from "prop-types";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

// Icons
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import MoreIcon from '@material-ui/icons/MoreVert';

import './header.scss';

export default class HeaderSecondary extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            anchorEl : null,
        }
    }

    moreButtonOpen = (e) => {
        e.preventDefault()
        this.setState({
            anchorEl : e.currentTarget,
        });
    }

    moreButtonClose = () => {
        this.setState({
            anchorEl : null,
        });
    }

    render() {
        let positionValue = this.props.position !== undefined ? this.props.position: "sticky";
        let mainButton = null;
        let closeButton = null ;
        let moreButton = null ;

        // Main Button
        if(this.props.MenuButton) {
            mainButton = 
            <IconButton edge="start"  color="inherit" aria-label="menu" onClick={this.props.mobileNav(true)}>
                <MenuIcon />
            </IconButton>
        } else if(this.props.moreButton.backButton) {
            mainButton = 
            <IconButton  edge="start" color="inherit" aria-label="menu" onClick={this.props.history.goBack}>
                <ArrowBackIosIcon />
            </IconButton>
        }  else if(this.props.moreButton.slideButton) {
            mainButton = 
            <IconButton  edge="start"  color="inherit" aria-label="menu" onClick={this.props.backButton}>
                <KeyboardArrowDownIcon />
            </IconButton>
        } else {
            mainButton = <span></span>
        }

        // Extra Button
        if(this.props.close !== undefined) {
            closeButton = 
            <IconButton  edge="end"  color="inherit" aria-label="menu" onClick={this.props.close}>
                <CloseIcon />
            </IconButton>
        }

        if (this.props.moreButton.moreIcon) {
            
            moreButton = 
            <React.Fragment>
                <IconButton edge="end" aria-controls="simple-menu" aria-haspopup="true" aria-label="display more actions" color="inherit" onClick={this.moreButtonOpen}>
                    <MoreIcon />
                </IconButton>
                <Menu
                    id="simple-menu"
                    anchorEl={this.state.anchorEl}
                    keepMounted
                    open={Boolean(this.state.anchorEl)}
                    onClose={this.moreButtonClose}
                >
                    <MenuItem onClick={this.moreButtonClose}>Profile</MenuItem>
                    <MenuItem onClick={this.moreButtonClose}>My account</MenuItem>
                    <MenuItem onClick={this.moreButtonClose}>Logout</MenuItem>
            </Menu>
          </React.Fragment>
        }


        return (
            <AppBar position={positionValue} className={this.props.inSlide ? "header-secondary header-secondary--slider" : "header-secondary"}>
                <Toolbar>
                    {mainButton}
                    <div className="header-main_controller">
                        {closeButton}
                        {moreButton}
                    </div>
                </Toolbar>
            </AppBar>
        );
    }

    static propTypes = {
        fixed: PropTypes.bool
    };

}

