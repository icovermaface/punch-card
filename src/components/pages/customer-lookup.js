import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import List from '@material-ui/core/List';
import MoreIcon from '@material-ui/icons/MoreVert';
import TextField from '@material-ui/core/TextField';

// Icon
import ArrowBackIosOutlinedIcon from '@material-ui/icons/ArrowBackIosOutlined';

import '../pages/customer-lookup.scss';
// import '../common/header.scss'

export default class SearchResult extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            isProcessing: false,
            customer: []
        }
    }

    componentDidMount = () => {
        this.fetchDetail().then(customer => {
            this.setState({
                customer: customer
            });
        });
    }

    fetchDetail = () => {
        return fetch("https://jsonplaceholder.typicode.com/users")
        .then(res => res.json())
        .then(
            (result) => {
                return Promise.resolve(result);
            },
            (error) => {
                console.log(error)
            }
        )  
    }

    handleListClick = (customer) => {
        console.log(customer)
        this.props.history.push({
            pathname: "/customer/" + customer.id,
            state: {
                customer
            }
        });
    }

    renderList = () => {
        let list = [];
        

        this.state.customer.forEach(customer => {
            list.push(
 
                <ListItem button key={customer.id}  onClick={this.handleListClick.bind(this, customer)}>
                    <ListItemText primary={customer.name} />
                </ListItem>
                
            )
        });

        return list
    }

    goBack = (e) => {
        e.preventDefault()
        this.props.history.goBack()
    }

    render = () => {
        return (
            <React.Fragment>
            <AppBar position="fixed" className="header_sub">
                <Toolbar >
                <IconButton  edge="start" color="inherit" aria-label="menu" onClick={this.goBack}>
                            <ArrowBackIosOutlinedIcon />
                </IconButton> 
                <div className="header_sub-bottom">
                    <Typography  variant="h5" noWrap className="header-title">
                    Look Up
                    </Typography>
                  
                </div>
                <IconButton aria-label="display more actions" edge="end" color="inherit">
                    <MoreIcon />
                </IconButton>
                </Toolbar>
            </AppBar>
            <main id="p-customer-lookup" className="sub-page">
            <TextField fullWidth id="outlined-search" label="Search field" type="search" variant="outlined" />
     
                <section>

                    <List component="nav" className="customer-lookup_list">
                        {this.renderList()}
                        {this.renderList()}
                    </List>

                </section> 
            </main>
           </React.Fragment>
        );
  }
}
