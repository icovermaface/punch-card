import React, { Component } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Toolbar from '@material-ui/core/Toolbar';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import MoreIcon from '@material-ui/icons/MoreVert';

// Icons
import ArrowBackIosOutlinedIcon from '@material-ui/icons/ArrowBackIosOutlined';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {  faTicketAlt, faIceSkate, faBarcodeRead } from '@fortawesome/pro-regular-svg-icons'

import '../pages/customer.scss';
// import '../layout/header.scss'

export default class Customer extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            isProcessing: false,
            customer: {}
        }
    }

    render = () => {
        return (
            <React.Fragment>
                
                <AppBar  className="header_sub header_dropshadow">
                    <Toolbar >
                    <IconButton edge="start" color="inherit" aria-label="menu" onClick={this.goBack}>
                                <ArrowBackIosOutlinedIcon />
                    </IconButton> 
                    
                    <div className="header_sub-bottom">
                        <Typography  variant="h5" noWrap className="header-title">
                            {this.state.customer.name}
                        </Typography>
                        <p className="header-description">{this.state.customer.email}</p>
                        <div className="customer_quick-detail">
                            <div className="quick-detail-item credit">
                                <div className="item-icon"><FontAwesomeIcon icon={faTicketAlt} size="lg"/></div>
                                <div className="item-text">30 <span>credit</span></div>
                            </div>
                            <div className="quick-detail-item credit">
                                <div className="item-icon"><FontAwesomeIcon icon={faIceSkate} size="lg" /></div>
                                <div className="item-text">3 <span>Skates</span></div>
                            </div>
                        </div>
                    </div>
                    
                    <IconButton aria-label="display more actions" edge="end" color="inherit">
                        <MoreIcon />
                    </IconButton>
                    </Toolbar>
                </AppBar>
                
              
                <main id="p-customer" className="sub-page">
                    
                    <section className="s-recent-transaction">
                        <Typography variant="h6" component="h4" >
                            Status
                        </Typography>
                        <Paper className="MuiPaper-elevation--custom" >
                            <List dense={true}>
                            
                                <ListItem>
                                    <ListItemAvatar>
                                        <div className="item-icon"><FontAwesomeIcon icon={faIceSkate} size="lg" /></div>
                                    </ListItemAvatar>
                                    <ListItemText
                                        primary="29292929292929"
                                        secondary="October 1"
                                    />
                                    <ListItemSecondaryAction>
                                        <p>Dropped Off</p>
                                    </ListItemSecondaryAction>
                                </ListItem>

                                <ListItem>
                                    <ListItemAvatar>
                                        <div className="item-icon"><FontAwesomeIcon icon={faIceSkate} size="lg" /></div>
                                    </ListItemAvatar>
                                    <ListItemText
                                        primary="S29292929292929"
                                        secondary="October 1"
                                    />
                                    <ListItemSecondaryAction>
                                        <p>Dropped Off</p>
                                    </ListItemSecondaryAction>
                                </ListItem>

                              
                            </List>
                        </Paper>


                        {/* <div className="item-list_wrapper drop-shadow">
                            <div className="item-list">
                                <div className="item-list-left">
                                    <div className="item-icon"><FontAwesomeIcon icon={faIceSkate} size="lg" /></div>
                                    <div className="item-text"> 
                                        <h5 className="item-text_id">ID: 29292929292929</h5>
                                        <p className="item-text_drop-off">Oct 20th </p>
                                    </div>
                                </div>
                                <div className="item-list-right">
                                    <p className="item-text_pick-up">Nov 1st</p>
                                </div>
                            </div>

                            <div className="item-list">
                                <div className="item-list-left">
                                    <div className="item-icon"><FontAwesomeIcon icon={faIceSkate} size="lg" /></div>
                                    <div className="item-text"> 
                                        <h5 className="item-text_id">ID: 29292929292929</h5>
                                        <p className="item-text_drop-off">Oct 20th </p>
                                    </div>
                                </div>
                                <div className="item-list-right">
                                    <p className="item-text_pick-up">pending</p>
                                </div>
                            </div>
                        </div> */}
                    </section>
                    {/* <section className="s-recent-transaction">
                        <h4>Status</h4>
                        <div className="item-list_wrapper drop-shadow">
                            <div className="item-list">
                                <div className="item-list-left">
                                    <div className="item-icon"><FontAwesomeIcon icon={faIceSkate} size="lg" /></div>
                                    <div className="item-text"> 
                                        <h5 className="item-text_id">ID: 29292929292929</h5>
                                        <p className="item-text_drop-off">Oct 20th </p>
                                    </div>
                                </div>
                                <div className="item-list-right">
                                    <p className="item-text_pick-up">Nov 1st</p>
                                </div>
                            </div>

                            <div className="item-list">
                                <div className="item-list-left">
                                    <div className="item-icon"><FontAwesomeIcon icon={faIceSkate} size="lg" /></div>
                                    <div className="item-text"> 
                                        <h5 className="item-text_id">ID: 29292929292929</h5>
                                        <p className="item-text_drop-off">Oct 20th </p>
                                    </div>
                                </div>
                                <div className="item-list-right">
                                    <p className="item-text_pick-up">pending</p>
                                </div>
                            </div>
                        </div>
                    </section> */}
                    <Button href="#text-buttons" className="history">History</Button>
                </main>
                <Button variant="info" className="btn-action-button btn-shadow"><FontAwesomeIcon icon={faBarcodeRead} size="lg" /></Button>
            </React.Fragment>
        );
  }
}
