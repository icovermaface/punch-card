import React, { Component } from 'react';
import PropTypes from "prop-types";
import Paper from "@material-ui/core/Paper"
import Grid from "@material-ui/core/Grid"
import TextField from "@material-ui/core/TextField"
import Container from "@material-ui/core/Container"
import Typography from "@material-ui/core/Typography"
import Button from "@material-ui/core/Button"
import Slide from '@material-ui/core/Slide';

import LayoutCustomerProfile from './components/layout/layout-customer-profile';
import LayoutCustomerList  from './components/layout/layout-customer-list';

openSlider = (type, data) => {
    this.setState({
        camera:"off",
        slider: {
            show: true, 
            type: type,
            data: data
        }
    });
}

closeSlider = () => {
    console.log("global state close")
    this.setState({
        camera: "on",
        slider: {
            show: false, 
            type: "",
            data: "",
            
        }
    }, () => {
        console.log("test")
    });
}

renderSliderContent = () => {
    let output = null;
   
    if (this.state.slider.type === "users" | this.state.slider.type === "customer-profile") {
        output =  <LayoutCustomerProfile data={this.state.slider} get={this.get} update={this.update} />
    } else if (this.state.slider.type === "customer-list") {
        output = 
        <React.Fragment>
            <Typography  variant="h4" noWrap className="layout-header_title">
                    Customers
            </Typography>
            <LayoutCustomerList data={this.state.slider} get={this.get}/>
        </React.Fragment>      
    }

    return (
       output
    )
}
