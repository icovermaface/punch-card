import React, { Component } from 'react';
import PropTypes from "prop-types";
import Paper from "@material-ui/core/Paper"
import Grid from "@material-ui/core/Grid"
import TextField from "@material-ui/core/TextField"
import Container from "@material-ui/core/Container"
import Typography from "@material-ui/core/Typography"
import Button from "@material-ui/core/Button"
import Slide from '@material-ui/core/Slide';


//compponent
import ScannerComponent  from './../layout/layout-scanner'
import LayoutCustomerProfile from './../layout/layout-customer-profile';
import LayoutCustomerList  from './../layout/layout-customer-list';


import '../pages/home.scss';

export default class Home extends Component {

    constructor(props) {
        super(props);

        this.state = {
            left: false,
            search: "",
            customer:[],
            scanner: {
                camera: "",
                barcode: {
                    success: "" , 
                    response: {}
                }
            },
            slider: {
                show: false, 
                type:"",
                data:""
            }
        }
    }

    componentDidMount = () => {
        // this.props.openSlider("customer-profile", {"barcodes":[{"code":"077052945621","format":"128","status":null},{"code":"014522778112","format":"128","status":null}],"_id":"5e35d088a532b70002891659","first_name":"Richard","last_name":"Chang","email":"richard@vivanaturals.com","credits":3,"__v":0})
        this.props.get("customers", null).then(customer => {
            this.setState({
                customer: customer
            });
        });
    }

    // Testing Profile Slider
    // componentDidMount = () => {
    //     this.props.openSlider("customer-profile", {"barcodes":[{"code":"077052945621","format":"128","status":null},{"code":"014522778112","format":"128","status":null}],"_id":"5e35d088a532b70002891659","first_name":"Richard","last_name":"Chang","email":"richard@vivanaturals.com","credits":3,"__v":0})
    // }

    onDetect= (params) =>{
       
        this.props.get("barcodes", params).then(barcodeResponse => {
            console.log("detected")
            if (this.props.globalState.isMobile) {
                this.props.get("customers", barcodeResponse).then(responses => {
            
                    if(responses.hasOwnProperty("error")) {
                        this.setState({
                            scanner: {
                                ...this.state.scanner,
                                camera: "on",
                                barcode: {
                                    success: false , 
                                    response: {msg: "Customer not found"}
                                }
                            }
                        });
                    } else {
                        if (!this.state.slider.show) {
                            this.openSlider("customer-profile", responses)
                        }
                    }
                })
            }
        }).catch(err => {
            console.log(err)
        });
    }

    customerLookup = () => {
        this.props.get("customers", null).then(responses => {
            if (this.props.globalState.isMobile) {
                    if(responses.hasOwnProperty("error")) {
                        this.setState({
                            scanner: {
                                camera: "on",
                                success: false , 
                                response: {msg: "Customer not found"}
                            }
                        });
                    } else {
                        this.openSlider("customer-list", responses)
                    }
            }
        }).catch(err => {
            console.log(err)
        });
    }

    openSlider = (type, data) => {
        console.log('open slider')
        this.setState({
            scanner: {
                camera: "off",
                barcode: {
                    success: true , 
                    response: data
                }
            },
            slider: {
                show: true, 
                type: type,
                data: data
            }
        });
    }
    
    closeSlider = () => {
        this.setState({
            scanner: {
                ...this.state.scanner,
                camera: "on"
            },
            slider: {
                show: false, 
                type: "",
                data: "",
                
            }
        }, () => {
            console.log("test")
        });
    }
    
    renderSliderContent = () => {
        let output = null;
       
        if (this.state.slider.type === "users" | this.state.slider.type === "customer-profile") {
            output =  <LayoutCustomerProfile data={this.state.slider.data} get={this.props.get} update={this.props.update} />
        } else if (this.state.slider.type === "customer-list") {
            output = 
            <React.Fragment>
                <Typography  variant="h4" noWrap className="layout-header_title">
                        Customers
                </Typography>
                <LayoutCustomerList 
                    data={this.state.slider} 
                    get={this.props.get} 
                    update={this.props.update} />
            </React.Fragment>      
        }
    
        return (
           output
        )
    }
    
    handleSearchInput = (value) => {
        this.setState({
            search: value
        });
    }
    
    handleSearchSubmit = (e) => {
        console.log('search')
        
        this.props.get("customers", null).then(responses => {
            
        }).catch(err => {
            console.log(err)
        });
    }

    handleInputChange = (e) => {
        e.preventDefault();
        const inputName = e.target.name;
        const inputValue = e.target.value;

        if (inputName === "search") {
            this.handleSearchInput(inputValue);
        } else {
            this.setState({
                [inputName]: inputValue
            });
        }
    };    

    renderMobile = () => {
        let sliderOverlay = null;        
        // Slider Overlay
        if(this.state.slider.show) {
            sliderOverlay = <div className="slider-overlay" onClick={this.closeSlider}>Overlay</div>
        }
        return (
            <React.Fragment>
                {sliderOverlay}
                <main id="p-home" className={this.props.viewport}>
                    <ScannerComponent {...this.props} globalState={this.props.globalState} onDetect={this.onDetect} customerLookup={this.customerLookup} scanner={this.state.scanner}/>
                    <Slide direction="up" in={this.state.slider.show} mountOnEnter unmountOnExit className="slide-up_result">
                        <Paper elevation={4} >
                            <div className="slide-up_close" onClick={this.closeSlider}></div>
                            
                            {this.renderSliderContent()}
                        </Paper>
                    </Slide> 
                </main>
            </React.Fragment>
        )
    }

    renderDesktop = () => {
        return (
            <Container id="p-home" className={this.props.viewport} component="main" maxWidth="md">
                <Grid container direction="row">
                    {/* <div onClick={slideUp ? this.closeSlide : null} className="slide-up_overlay"></div> */}
                    <Grid item xs={12} md={12}>
                        <Typography  variant="h4" noWrap className="layout-header_title">
                                Customers
                        </Typography>
                        <Paper className="MuiPaper-elevation--custom search-tool">                    
                            <TextField fullWidth id="outlined-search" name="search" label="Barcode, name, email" type="search" variant="outlined" onChange={this.handleInputChange} />
                            <div className="search-button">
                                <Button 
                                    onClick={this.handleSearchSubmit}
                                    variant="contained" 
                                    color="primary"
                                    size="small"
                                >
                                    Search
                                </Button>
                            </div>
                        </Paper>
                        <LayoutCustomerList 
                            customers={this.state.customer} 
                            handleDialogClose={this.props.handleDialogClose} 
                            handleDialogOpen={this.props.handleDialogOpen} 
                        />
                    </Grid>
                </Grid>
            </Container>
        )
    }

    render() {
        console.log(this.state.search)
        let output = null;

            if (this.props.globalState.isMobile) {
                output = this.renderMobile();
            } else {
                output = this.renderDesktop();
            }

        return (output);
    }

}