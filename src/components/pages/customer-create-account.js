import React, { Component } from 'react';
import LayoutCreateCustomerAccount from './../layout/layout-customer-create-account'

export default class CustomerCreateAccount extends Component {

    render = () => {
        return (
            <LayoutCreateCustomerAccount 
            {...this.props.state}
            isProcessing={this.props.isProcessing}   
            closeDialog={this.props.handleDialogClose}
            create={this.props.create}
            />
        )
    }
}
