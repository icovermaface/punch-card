import React, { Component } from 'react';
import Quagga from '@ericblade/quagga2';
import Header from './../common/header';
import HeaderSecondary from './../common/header-secondary';
import Paper from '@material-ui/core/Paper';
import Slide from '@material-ui/core/Slide';
import ScannerLayoutUsers from '../layout/layout-customer-profile';
import Button from '@material-ui/core/Button';

// Icons 
import KeyboardOutlinedIcon from '@material-ui/icons/KeyboardOutlined';
// CSS
import '../pages/scanner.scss';

export default class Scanner extends Component {
    
    constructor(props) {
        super(props);

        this.state = {
            nav:{
                menu: true, 
                close:  true
            },
            scannerStart : false, 
            scannerBarcode: "",
            alertMsg: "",
            slideIn: ""
        }
        
    }

    componentDidMount = () => {
        this.startCamera()
    }

    componentWillUnmount = () => {
        Quagga.stop();
    }

    get = (endpoint, params) => {

        if (endpoint === "users") {
          
            return fetch("https://jsonplaceholder.typicode.com/users/" + params.id)
            .then(res => res.json())
            .then(
                (result) => {
                    console.log(result)
                    return Promise.resolve(result);
                },
                (error) => {
                    this.setState({
                        slideIn: "",
                        alertMsg: "There is no user for this",
                    });
                    console.log(error)
                }
            )  
        } else if (endpoint === "search") {
            return fetch("https://jsonplaceholder.typicode.com/users")
            .then(res => res.json())
            .then(
                (result) => {
                    return Promise.resolve(result);
                },
                (error) => {
                    console.log(error)
                }
            )  
        }
    }

    // Navigation
    navClose = () => {
        Quagga.stop()
        this.props.history.goBack()
    }

    // Camera
    stopCamera = () => {
        Quagga.stop();
    }

    startCamera = () => {
        let vWidth = "640"
        let vHeight = "480"
        
        this.setState({
            scannerStart: true,
            slideIn: ""
        });

        // let vWidth = this.props.isMobile ? window.innerWidth : window.innerWidth
        // let vHeight = this.props.isMobile ? window.innerWidth : window.innerHeight
        Quagga.init({
            inputStream : {
                name : "Live",
                type : "LiveStream",
                target: document.getElementById("camera_viewport"),
                constraints: {
                    width: vWidth ,
                    height: vHeight
                }
            },
            area: { // defines rectangle of the detection/localization area
                top: "0%",    // top offset
                right: "0%",  // right offset
                left: "0%",   // left offset
                bottom: "0%"  // bottom offset
            },
            locate: true,
            decoder: {
                readers: ["code_128_reader", "upc_reader", "upc_e_reader"]
            }
          }, function(err) {
                if (err) {
                    console.log(err);
                    return
                }
                console.log("Initialization finished. Ready to start");
                Quagga.start();
        });
        Quagga.onDetected(this.onDetect)
    }

    onDetect= (res) =>{
        console.log('detected')
        Quagga.stop()
        Quagga.offProcessed()
        
        this.setState({
            slideIn: "user",
            scannerBarcode: res.codeResult.code
        });
        // this.props.onBarcodeDetect(res.codeResult.code)
    }
    
    // Action 
    onComplete = () => {
        this.setState({
            slideIn: "" 
        }, this.startCamera);
    }

    handleChange = (value) => {
        this.setState({
            slideIn: value
        });
    };

    closeSlide = () => {
        this.setState({
            slideIn: ""
        },this.startCamera());
    }

    showSearch =() => {
        this.setState({
            slideIn: "search",
            scannerBarcode: "12382818937928937",
        });
    }

    render() {
        let slideUp = this.state.slideIn !== "" ? true : false;
        let classes = slideUp ? "slider-up" : null
        let scannerMsg = null;

            if (this.state.alertMsg !== "") {
                scannerMsg = 
                    <React.Fragment>
                        <p className="scanner-msg">{this.state.alertMsg}</p>
                    </React.Fragment>
            }

        let content = null;

            if(this.state.slideIn === "user") {
                content = 
                        <React.Fragment>
                            <HeaderSecondary {...this.props} isMobile={this.props.mobileNav} moreButton={{ moreIcon:true  }} />
                            <ScannerLayoutUsers onComplete={this.onComplete} id={this.state.scannerBarcode} get={this.get} inSlide={true} post={"this.update"} />
                            {/* <Button color="primary"  className="slide-up_result-complete" onClick={this.onComplete}>Finished</Button> */}
                        </React.Fragment>
            } else {
                content = "";
            }

        return (
            <main id="p-scanner" className={classes} >

                <Header {...this.props} isMobile={this.props.mobileNav} moreButton={this.state.nav} close={this.navClose}/>
                <div onClick={slideUp ? this.closeSlide : null} className="slide-up_overlay"></div>
                <Slide direction="up" in={slideUp} mountOnEnter unmountOnExit className="slide-up_result">
                    <Paper elevation={4} >
                        <div className="slide-up_close" onClick={this.closeSlide}></div>
                        {content}
                    </Paper>
                </Slide>
                
                <div className="scanner-camera">
                    <div className="camera-overlay_top"></div>
                    <div id="camera_viewport"></div>
                    <div className="camera-overlay_bottom"></div>
                </div>
                
                <div className="scanner-msg">
                    {scannerMsg}
                </div>

                <div className="footer_controller dark-theme" >
                    <Button className="keyboard" onClick={this.showSearch} >
                        <KeyboardOutlinedIcon />
                    </Button>
                </div>
                
            </main>
        );
    }
    
}