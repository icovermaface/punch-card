import React, { Component } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'
import FormControl from '@material-ui/core/FormControl'
import Container from '@material-ui/core/Container';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

import '../pages/login.scss';

export default class Login extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isProcessing: false,
            email: "richard@gmail.com",
            store: "store-2",
            password:"",
            remember_me:"",
            modal: null,
            isModalProcessing: false
        }
    }

    handleInputChange = (e) => {
        e.preventDefault();
        const inputName = e.target.name;
        const inputValue = e.target.value;
        this.setState({[inputName]: inputValue});
    }

    handleSubmit = (e) => {
        e.preventDefault();
        this.setState({isProcessing: true});
        const params = {
            email: this.state.email,
            store: this.state.store, 
            password: this.state.password,
            remember_me: this.state.remember_me
        }

        this.props.get("/order/verify", params)
        .then(Response => (this.props.get))
        .then(Response => {
          console.log("correct email");
          this.props.history.push({
                pathname: "/home",
                state:{}
            });
          return true;
        })
        .catch(err => {
            console.log("incorrect email")
        })
        
    }
    
    render() {


        return (
            <Box id="p-login">
                <Container   component="main" maxWidth="xs">
                        
                        <div className="login_company-logo"></div>
                        <Typography component="h1" variant="h5">
                            <span>Company Name</span> 
                        </Typography>
                        <form className="form" onSubmit={this.handleSubmit}>
                                            <Grid container spacing={1}>
                                                <Grid item xs={12} sm={8}>    
                                                    <TextField
                                                        variant="outlined"
                                                        margin="normal"
                                                        required
                                                        fullWidth
                                                        id="email"
                                                        label="Email Address"
                                                        name="email"
                                                        autoComplete="email"
                                                        autoFocus
                                                        value={this.state.email}
                                                        onChange={this.handleInputChange}
                                                    />
                                                </Grid>
                                                <Grid item xs={12} sm={4}>    
                                                    <FormControl variant="outlined" fullWidth className="login_store">
                                                            <InputLabel ref={"Label"} id="store">
                                                            Store
                                                            </InputLabel>
                                                            <Select
                                                            variant="outlined"
                                                            labelId="store"
                                                            id="sotre"
                                                            value={this.state.store}
                                                            labelWidth={40}
                                                            onChange={this.handleInputChange}
                                                            // onChange={handleChange}
                                                            // labelWidth={labelWidth}
                                                            >
                                                            <MenuItem value={'store-1'}>Store 1</MenuItem>
                                                            <MenuItem value={'store-2'}>Store 2</MenuItem>
                                                            <MenuItem value={'store-3'}>Store 3</MenuItem>
                                                            </Select>
                                                    </FormControl>
                                                </Grid>
                                    
                                                <Grid item xs={12}>
                                                <TextField
                                                        variant="outlined"
                                                        margin="normal"
                                                        required
                                                        fullWidth
                                                        name="password"
                                                        label="Password"
                                                        type="password"
                                                        id="password"
                                                        autoComplete="current-password"
                                                        value={this.state.password}
                                                        onChange={this.handleInputChange}
                                                    />
                                                </Grid>
                                                <Grid item xs={12}>
                                                    <Button
                                                        type="submit"
                                                        fullWidth
                                                        variant="contained"
                                                        color="primary"
                                                        size="large"
                                                        className="submit btn-gradient btn-shadow"
                                                        disabled={this.state.isProcessing}
                                                    >
                                                    Sign In
                                                    </Button>
                                                </Grid>


                                                {/* <Grid item xs>
                                                    <FormControlLabel
                                                        control={<Checkbox value="remember" color="primary" />}
                                                        label="Remember me"
                                                        value={this.state.remember_me} onChange={this.handleInputChange}
                                                    />
                                                </Grid>
                                                <Grid item xs className="login_forgot-bassword">
                                                    <Link href="#" variant="body2">
                                                    Forgot password?
                                                    </Link>
                                                </Grid> */}
                                                
                                            </Grid>
                                        
                                            
                                        </form>
                </Container>   
            </Box>   
        );
    }
    
}