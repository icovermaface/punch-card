import React, { Component } from 'react';
import Button from '@material-ui/core/Button'
import Fab from '@material-ui/core/Fab';

// Icons
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faBarcodeScan} from '@fortawesome/pro-regular-svg-icons'
import KeyboardOutlinedIcon from '@material-ui/icons/KeyboardOutlined';

import '../pages/home.scss';

export default class Home extends Component {

    constructor(props) {
        super(props);

        this.state = {
            left: false,
        }
    }
    
    toScanner = () => {
        this.props.history.push({
            pathname: "/scanner",
        });
    }

    openSearch = () => {
        this.props.history.push({
            pathname: "/customer-lookup",
        });
    }

    render() {
        return (
            <main id="p-home">
                <div className="tap-to-scan_wrapper">
                    <div>
                        <div className="tap-to-scan_palse"></div>
                        <Fab className="tap-to-scan" aria-label="scan" onClick={this.toScanner}>
                            <FontAwesomeIcon icon={faBarcodeScan} />
                        </Fab>
                    </div>
                    <p className="tap-to-scan_text">Tap to scan barcode</p>
                </div>
                <div className="footer_controller">
                    <Button className="keyboard" onClick={this.openSearch}  disabled={this.state.isProcessing}>
                        <KeyboardOutlinedIcon />
                    </Button>
                </div>
            </main>
        );
    }
    
}